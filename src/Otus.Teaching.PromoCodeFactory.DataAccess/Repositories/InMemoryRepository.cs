﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var deleted = Data.FirstOrDefault(x => x.Id == id);
            if (deleted == null)
                return Task.FromResult(false);
            Data = Data.Where(x => x != deleted).ToArray();
            return Task.FromResult(true);
        }
        public Task<bool> UpdateAsync(T entity)
        {
            var deleted = Data.FirstOrDefault(x => x.Id == entity.Id);
            if (deleted == null)
                return Task.FromResult(false);
            Data = Data
                .Where(x => x != deleted)
                .Concat(new []{entity})
                .ToArray();
            return Task.FromResult(true);
        }

        public Task<Guid> Create(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data = Data
                .Concat(new[] {entity})
                .ToArray();
            return Task.FromResult(entity.Id);
        }
    }
}