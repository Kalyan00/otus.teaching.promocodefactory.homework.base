﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            if (!await _employeeRepository.DeleteByIdAsync(id))
                return NotFound();
            return Ok();
        }


        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPatch("{id:guid}")]
        public async Task<ActionResult> UpdateEmployeeByIdAsync(Guid id,[FromBody] EmployeeUpdate employee)
        {
            var emp = await _employeeRepository.GetByIdAsync(id);
            if(emp == null) 
                return NotFound();

            UpdateEmployee(employee, emp);
            
            if (!await _employeeRepository.UpdateAsync(emp))
                return BadRequest();

            return Ok();
        }

        private static void UpdateEmployee(EmployeeUpdate employee, Employee emp)
        {
            emp.FirstName = employee.FirstName ?? emp.FirstName;
            emp.LastName = employee.LastName ?? emp.LastName;
            emp.Email = employee.Email ?? emp.Email;
        }

        /// <summary>
        /// Создать сотрудника 
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync([FromBody] EmployeeUpdate employee)
        {
            var emp = new Employee();
            UpdateEmployee(employee, emp);

            return await _employeeRepository.Create(emp);
        }
    }
}